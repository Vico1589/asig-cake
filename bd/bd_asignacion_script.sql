
--CREATE DATABASE asigancion;

CREATE TABLE estado (
  id INT NOT NULL,
  estado VARCHAR(20) NOT NULL,
  CONSTRAINT pk_estado PRIMARY KEY (id)
);

CREATE TABLE tipo(
  id INT NOT NULL,
  tipo VARCHAR(20)NOT NULL,
  CONSTRAINT pk_tipo PRIMARY KEY (id)
);

CREATE TABLE usuario(
  id INT NOT NULL,
  correo VARCHAR(50) NOT NULL,
  aPaterno VARCHAR(50) NOT NULL,
  aMaterno VARCHAR(50),
  nombre VARCHAR(50) NOT NULL,
  password VARCHAR(60) NOT NULL,
  estado INT NOT NULL,
  tipo INT NOT NULL,
  UNIQUE (correo),
  CONSTRAINT pk_usuario PRIMARY KEY (id)
);

ALTER TABLE usuario ADD CONSTRAINT fk_usuario_estado FOREIGN KEY (estado)
REFERENCES estado (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuario ADD CONSTRAINT fk_usuario_tipo FOREIGN KEY (tipo)
REFERENCES tipo (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE SEQUENCE estado_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE estado ALTER COLUMN id SET DEFAULT nextval('estado_seq');

CREATE SEQUENCE tipo_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE tipo ALTER COLUMN id SET DEFAULT nextval('tipo_seq');

CREATE SEQUENCE usuario_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE usuario ALTER COLUMN id SET DEFAULT nextval('usuario_seq');


INSERT INTO estado (estado) VALUES('activo');
INSERT INTO estado (estado) VALUES('inactivo');

INSERT INTO tipo (tipo) VALUES('administrador');
INSERT INTO tipo (tipo) VALUES('usuario');

INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('admin@correo.mx', 'Admin', 'Admin', 'Admin', '$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa', 1, 1);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('user@correo.mx', 'User', 'User', 'User', '$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa', 1, 2);