--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

-- Started on 2019-01-29 15:41:22

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12278)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 199 (class 1259 OID 16669)
-- Name: estado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16642)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado (
    id integer DEFAULT nextval('public.estado_seq'::regclass) NOT NULL,
    estado character varying(20) NOT NULL
);


ALTER TABLE public.estado OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 17093)
-- Name: movimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_seq OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17073)
-- Name: movimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento (
    id integer DEFAULT nextval('public.movimiento_seq'::regclass) NOT NULL,
    movimiento character varying(50) NOT NULL
);


ALTER TABLE public.movimiento OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 17134)
-- Name: registro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registro_seq OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 17119)
-- Name: registro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registro (
    id integer DEFAULT nextval('public.registro_seq'::regclass) NOT NULL,
    usuario integer NOT NULL,
    movimiento integer NOT NULL,
    fecha timestamp with time zone
);


ALTER TABLE public.registro OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16672)
-- Name: tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_seq OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16647)
-- Name: tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo (
    id integer DEFAULT nextval('public.tipo_seq'::regclass) NOT NULL,
    tipo character varying(20) NOT NULL
);


ALTER TABLE public.tipo OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16675)
-- Name: usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_seq OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16652)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id integer DEFAULT nextval('public.usuario_seq'::regclass) NOT NULL,
    correo character varying(50) NOT NULL,
    apaterno character varying(50) NOT NULL,
    amaterno character varying(50),
    nombre character varying(50) NOT NULL,
    password character varying(60) NOT NULL,
    estado integer NOT NULL,
    tipo integer NOT NULL,
    photo character varying(255),
    photo_dir character varying(255)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 2191 (class 0 OID 16642)
-- Dependencies: 196
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado (id, estado) FROM stdin;
1	Activo
2	Inactivo
\.


--
-- TOC entry 2197 (class 0 OID 17073)
-- Dependencies: 202
-- Data for Name: movimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movimiento (id, movimiento) FROM stdin;
1	Creación de usuario
2	Modificación de usuario
4	Borrado de usuario
5	Acceso al sistema
6	Salida del sistema
7	Cambio de contraseña
8	Solicitud de cambio de contraseña
3	Registro de usuario
\.


--
-- TOC entry 2199 (class 0 OID 17119)
-- Dependencies: 204
-- Data for Name: registro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registro (id, usuario, movimiento, fecha) FROM stdin;
1	90	5	2019-01-23 17:13:02.982085-06
2	90	5	2019-01-24 14:09:48.988431-06
4	90	6	2019-01-24 14:16:31.578116-06
5	90	5	2019-01-24 14:17:09.658548-06
6	90	6	2019-01-24 14:17:12.949508-06
7	90	5	2019-01-24 14:17:59.095195-06
8	90	6	2019-01-24 14:18:57.411363-06
9	90	5	2019-01-24 14:23:53.39167-06
11	90	6	2019-01-24 14:57:51.094241-06
12	90	5	2019-01-24 14:58:08.242087-06
13	90	6	2019-01-24 15:23:53.867731-06
14	90	5	2019-01-24 16:02:59.022188-06
15	90	1	2019-01-24 16:08:30.396548-06
16	90	1	2019-01-24 16:39:14.074355-06
10	90	1	2019-01-24 14:25:53.39966-06
17	90	1	2019-01-24 16:54:19.260894-06
18	90	1	2019-01-24 16:58:41.924255-06
19	90	1	2019-01-24 17:25:08.349451-06
20	90	6	2019-01-24 17:51:29.832834-06
21	90	5	2019-01-25 13:18:13.457661-06
22	90	1	2019-01-25 13:30:56.076711-06
23	90	1	2019-01-25 13:38:44.824284-06
24	90	1	2019-01-25 13:55:05.493042-06
25	90	1	2019-01-25 14:07:09.015427-06
26	90	3	2019-01-25 15:07:55.0762-06
27	1	3	2019-01-25 15:12:31.380836-06
28	41	3	2019-01-25 15:13:38.066013-06
29	44	3	2019-01-25 15:16:11.976229-06
30	90	2	2019-01-25 15:28:16.522576-06
31	90	2	2019-01-25 15:29:25.054426-06
32	90	2	2019-01-25 15:48:46.855652-06
33	90	2	2019-01-25 15:52:32.32675-06
34	90	2	2019-01-25 15:54:40.112478-06
35	90	5	2019-01-25 16:44:37.733591-06
36	90	6	2019-01-25 17:05:06.143644-06
37	90	5	2019-01-28 11:50:03.711942-06
38	90	6	2019-01-28 11:53:31.087745-06
39	90	5	2019-01-28 12:33:22.392264-06
40	90	6	2019-01-28 13:01:34.30081-06
41	90	5	2019-01-28 13:08:54.293444-06
42	90	6	2019-01-28 13:13:00.757615-06
45	90	5	2019-01-28 13:16:39.283977-06
46	90	6	2019-01-28 13:53:14.355954-06
48	90	5	2019-01-28 15:18:32.77685-06
49	90	6	2019-01-28 15:21:53.370051-06
50	90	8	2019-01-28 15:22:10.663018-06
53	90	5	2019-01-28 15:33:25.627789-06
54	90	6	2019-01-28 15:36:39.685975-06
55	90	5	2019-01-29 12:38:10.984045-06
56	90	6	2019-01-29 12:52:25.550538-06
57	90	5	2019-01-29 12:52:47.023191-06
58	90	6	2019-01-29 12:53:03.580869-06
59	2	5	2019-01-29 12:53:19.730736-06
60	2	6	2019-01-29 13:09:26.578677-06
61	90	5	2019-01-29 13:09:46.389687-06
62	90	2	2019-01-29 13:11:23.741941-06
63	90	6	2019-01-29 13:11:58.432052-06
64	2	5	2019-01-29 13:12:07.990396-06
65	2	6	2019-01-29 13:14:46.995498-06
66	2	5	2019-01-29 13:14:58.686088-06
67	2	6	2019-01-29 13:18:09.75382-06
68	90	5	2019-01-29 13:18:18.540965-06
69	90	2	2019-01-29 13:18:54.253711-06
70	90	6	2019-01-29 13:18:59.624578-06
71	47	5	2019-01-29 13:19:13.599755-06
72	47	6	2019-01-29 13:20:38.293638-06
73	47	5	2019-01-29 13:20:47.390167-06
74	47	5	2019-01-29 13:22:25.89819-06
75	90	5	2019-01-29 13:22:50.527889-06
76	90	6	2019-01-29 13:32:30.996938-06
77	2	5	2019-01-29 13:32:47.519601-06
78	2	6	2019-01-29 13:42:49.931271-06
79	90	5	2019-01-29 13:43:00.128836-06
80	90	6	2019-01-29 14:14:34.078518-06
81	47	5	2019-01-29 14:14:49.923522-06
82	2	5	2019-01-29 14:15:04.072525-06
83	2	6	2019-01-29 14:20:33.619949-06
84	2	5	2019-01-29 14:20:47.698715-06
85	2	6	2019-01-29 14:22:26.896563-06
86	90	5	2019-01-29 14:22:55.973901-06
87	90	6	2019-01-29 14:42:45.593541-06
88	47	8	2019-01-29 14:43:40.234649-06
89	47	8	2019-01-29 14:45:13.535769-06
90	47	5	2019-01-29 14:45:56.285266-06
92	90	5	2019-01-29 14:48:49.341329-06
93	90	6	2019-01-29 14:49:28.239693-06
94	2	5	2019-01-29 15:17:20.176277-06
95	2	7	2019-01-29 15:17:44.429959-06
96	2	6	2019-01-29 15:17:48.820774-06
97	2	5	2019-01-29 15:18:02.939973-06
98	2	7	2019-01-29 15:20:46.706199-06
99	2	6	2019-01-29 15:20:52.342629-06
100	90	5	2019-01-29 15:25:34.583868-06
91	90	3	2019-01-29 14:47:53.916888-06
51	90	8	2019-01-28 15:24:45.896492-06
52	90	8	2019-01-28 15:25:40.211531-06
47	90	3	2019-01-28 15:14:18.915463-06
44	90	3	2019-01-28 13:15:39.972275-06
43	90	3	2019-01-28 13:13:11.250659-06
101	90	4	2019-01-29 15:32:58.242642-06
102	90	4	2019-01-29 15:33:09.761727-06
103	90	4	2019-01-29 15:33:19.03433-06
104	90	6	2019-01-29 15:33:39.241506-06
\.


--
-- TOC entry 2192 (class 0 OID 16647)
-- Dependencies: 197
-- Data for Name: tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo (id, tipo) FROM stdin;
1	Administrador
2	Usuario
\.


--
-- TOC entry 2193 (class 0 OID 16652)
-- Dependencies: 198
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, correo, apaterno, amaterno, nombre, password, estado, tipo, photo, photo_dir) FROM stdin;
41	ezio_assassin@ubisoft.com	Auditore	da Firenze	Ezio	$2y$10$mEqzxlju.yAEl1pQObeer.1bLA00.Yjz/EOO9A/Oltv3yccyzIK3e	1	1	ezio.jpg	b85b6495-3045-4fd1-8713-43d3750b3f17
40	masterchief@halo.com	Master		Chief	$2y$10$OO76y/ixXLwNdacJu7ofY.u7gZShBg7VxHVCeTptUG/29xN0pOqJi	1	1	\N	\N
42	god_of_war@gow.com	NA		Kratos	$2y$10$OhiwIix9VbrYL5WTQEew4.wqGb7xiakjMlRbP/Jl8Q7tOVxIbAofe	1	1	\N	\N
43	andrew.bigd@bioshock.com	Ryan		Andrew	$2y$10$JFkkxZi5tBducYB334JZ9uWjsquGQH0E988uDpctFvUBxfGUu7BFO	1	1	\N	\N
46	juliangm@gmail.com	García	Morales	Julián	$2y$10$raU8XtbTOz2cZ2FGDiHKXuFSf5Gwq0p0QPg.ZtYzlXJT23q6Oaq8u	1	2	\N	\N
45	geraltr@thewitcher.com	de Rivia		Geralt	$2y$10$jEtLu2kf6ExIFENMk0/KPePzFyKELIpMz4Wg7VLYL2CX7vbKWKsQ.	1	2	geralt.jpg	5c4f7051-f025-44b9-ba97-a0d6f99c5c75
39	nathand@uncharted.com	Drake		Nathan	$2y$10$rSwP1zj/hsf1f80pQlAYsOMMZHfFysWiWvtoWdpA22ZJkUpExi8JK	1	1	nathan.PNG	3677ee80-dd01-4348-b7e7-2e95d3183102
44	jhonnybravo@gmail.com	Bravo	Castro	Juan Manuel	$2y$10$1yWT8/ahRH4iJSbYPQkj5.5VM/DzlDCs1lBt6ufYDmivQIvKwp0cG	1	2	jb.png	8e9172ed-8d92-40a0-ade7-028a063f2849
1	admin@correo.mx	Admin	Admin	Admin	$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa	1	1	admin.png	a79d382a-0b4a-45e7-906e-ea2d79125d52
90	becario.victor.ruiz@gmail.com	Ruiz		Víctor	$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa	1	1	images.jpg	0b35c10b-ba3a-4dfa-af88-5b6f8745dd35
55	prueba@prueba.mx	Prueba		Prueba	$2y$10$sbX7J6flbrH2mkqv1mwlfu.n4foUjgKlNKGwvLu0avvmHK2qXW.5G	1	1	\N	\N
47	fff@gmail.com	Fernández	Fernández	Fernando	$2y$10$.wFPDMS0OiRIEWQkLOZmGO0a2j0Jd3Dv5pBn6FV35mYZ5ESFjv84.	2	2	user.png	7aea4c67-f5e3-4ef1-b60b-b554ba686f08
2	user@correo.mx	User	User	User	$2y$10$1iqaCaIXsCVB6z3YwPRbI.s9s3dKCywDZMruVFiPpt.Yuoyzqk9X6	1	2	user.png	2261fb79-17c3-4800-a584-8d9565f52bb2
\.


--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 199
-- Name: estado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_seq', 3, true);


--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 203
-- Name: movimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movimiento_seq', 8, true);


--
-- TOC entry 2211 (class 0 OID 0)
-- Dependencies: 205
-- Name: registro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registro_seq', 104, true);


--
-- TOC entry 2212 (class 0 OID 0)
-- Dependencies: 200
-- Name: tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_seq', 4, true);


--
-- TOC entry 2213 (class 0 OID 0)
-- Dependencies: 201
-- Name: usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_seq', 103, true);


--
-- TOC entry 2055 (class 2606 OID 16646)
-- Name: estado pk_estado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT pk_estado PRIMARY KEY (id);


--
-- TOC entry 2063 (class 2606 OID 17077)
-- Name: movimiento pk_movimiento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT pk_movimiento PRIMARY KEY (id);


--
-- TOC entry 2065 (class 2606 OID 17123)
-- Name: registro pk_registro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT pk_registro PRIMARY KEY (id);


--
-- TOC entry 2057 (class 2606 OID 16651)
-- Name: tipo pk_tipo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo
    ADD CONSTRAINT pk_tipo PRIMARY KEY (id);


--
-- TOC entry 2059 (class 2606 OID 16656)
-- Name: usuario pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id);


--
-- TOC entry 2061 (class 2606 OID 16658)
-- Name: usuario usuario_correo_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_correo_key UNIQUE (correo);


--
-- TOC entry 2069 (class 2606 OID 17129)
-- Name: registro fk_registro_movimiento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT fk_registro_movimiento FOREIGN KEY (movimiento) REFERENCES public.movimiento(id) MATCH FULL;


--
-- TOC entry 2068 (class 2606 OID 17124)
-- Name: registro fk_registro_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT fk_registro_usuario FOREIGN KEY (usuario) REFERENCES public.usuario(id) MATCH FULL;


--
-- TOC entry 2066 (class 2606 OID 16659)
-- Name: usuario fk_usuario_estado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk_usuario_estado FOREIGN KEY (estado) REFERENCES public.estado(id) MATCH FULL;


--
-- TOC entry 2067 (class 2606 OID 16664)
-- Name: usuario fk_usuario_tipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk_usuario_tipo FOREIGN KEY (tipo) REFERENCES public.tipo(id) MATCH FULL;


-- Completed on 2019-01-29 15:41:22

--
-- PostgreSQL database dump complete
--

