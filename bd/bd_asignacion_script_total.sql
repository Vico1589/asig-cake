
--CREATE DATABASE asigancion;
CREATE TABLE estado (
  id INT NOT NULL,
  estado VARCHAR(20) NOT NULL,
  CONSTRAINT pk_estado PRIMARY KEY (id)
);

CREATE TABLE tipo(
  id INT NOT NULL,
  tipo VARCHAR(20)NOT NULL,
  CONSTRAINT pk_tipo PRIMARY KEY (id)
);

CREATE TABLE usuario(
  id INT NOT NULL,
  correo VARCHAR(50) NOT NULL,
  aPaterno VARCHAR(50) NOT NULL,
  aMaterno VARCHAR(50),
  nombre VARCHAR(50) NOT NULL,
  password VARCHAR(60) NOT NULL,
  estado INT NOT NULL,
  tipo INT NOT NULL,
  photo VARCHAR(255),
  photo_dir VARCHAR(255),
  UNIQUE (correo),
  CONSTRAINT pk_usuario PRIMARY KEY (id)
);

CREATE TABLE movimiento (
  id INT NOT NULL,
  movimiento VARCHAR(50) NOT NULL,
  CONSTRAINT pk_movimiento PRIMARY KEY (id)
);

CREATE TABLE registro(
  id INT NOT NULL,
  usuario INT NOT NULL,
  movimiento INT NOT NULL,
  fecha TIMESTAMPTZ,
  CONSTRAINT pk_registro PRIMARY KEY (id)
);

ALTER TABLE usuario ADD CONSTRAINT fk_usuario_estado FOREIGN KEY (estado)
REFERENCES estado (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuario ADD CONSTRAINT fk_usuario_tipo FOREIGN KEY (tipo)
REFERENCES tipo (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE registro ADD CONSTRAINT fk_registro_usuario FOREIGN KEY (usuario)
REFERENCES usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE registro ADD CONSTRAINT fk_registro_movimiento FOREIGN KEY (movimiento)
REFERENCES movimiento (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE SEQUENCE estado_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE estado ALTER COLUMN id SET DEFAULT nextval('estado_seq');

CREATE SEQUENCE tipo_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE tipo ALTER COLUMN id SET DEFAULT nextval('tipo_seq');

CREATE SEQUENCE usuario_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE usuario ALTER COLUMN id SET DEFAULT nextval('usuario_seq');

CREATE SEQUENCE movimiento_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE movimiento ALTER COLUMN id SET DEFAULT nextval('movimiento_seq');

CREATE SEQUENCE registro_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE registro ALTER COLUMN id SET DEFAULT nextval('registro_seq');


INSERT INTO estado (estado) VALUES('Activo');
INSERT INTO estado (estado) VALUES('Inactivo');

INSERT INTO tipo (tipo) VALUES('Administrador');
INSERT INTO tipo (tipo) VALUES('Usuario');

INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('admin@email.mx', 'Admin', 'Admin', 'Admin', '$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa', 1, 1);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('user@email.mx', 'User', 'User', 'User', '$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('nathand@uncharted.com', 'Drake', '',  'Nathan', '$2y$10$rSwP1zj/hsf1f80pQlAYsOMMZHfFysWiWvtoWdpA22ZJkUpExi8JK', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('masterchief@halo.com',  'Master', '', 'Chief', '$2y$10$OO76y/ixXLwNdacJu7ofY.u7gZShBg7VxHVCeTptUG/29xN0pOqJi', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('ezio_assassin@ubisoft.com', 'Auditore',  'da Firenze',  'Ezio', '$2y$10$mEqzxlju.yAEl1pQObeer.1bLA00.Yjz/EOO9A/Oltv3yccyzIK3e', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('god_of_war@gow.com', 'NA', '', 'Kratos', '$2y$10$OhiwIix9VbrYL5WTQEew4.wqGb7xiakjMlRbP/Jl8Q7tOVxIbAofe', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('andrew.bigd@bioshock.com', 'Ryan', '', 'Andrew', '$2y$10$JFkkxZi5tBducYB334JZ9uWjsquGQH0E988uDpctFvUBxfGUu7BFO', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('jhonnybravo@gmail.com', 'Bravo', 'Castro',  'Juan Manuel', '$2y$10$1yWT8/ahRH4iJSbYPQkj5.5VM/DzlDCs1lBt6ufYDmivQIvKwp0cG', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('geraltr@thewitcher.com', 'de Rivia', '', 'Geralt', '$2y$10$jEtLu2kf6ExIFENMk0/KPePzFyKELIpMz4Wg7VLYL2CX7vbKWKsQ.', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('juliangm@gmail.com', 'García', 'Morales', 'Julián', '$2y$10$raU8XtbTOz2cZ2FGDiHKXuFSf5Gwq0p0QPg.ZtYzlXJT23q6Oaq8u', 1, 2);
INSERT INTO usuario(correo, aPaterno, aMaterno, nombre, password, estado, tipo) VALUES('fff@gmail.com', 'Fernández', 'Fernández', 'Fernando', '$2y$10$fNE8FfzW6PR8dBqt.SxDBeD3iNvUaIZMku54DC1FUuQU9GX50ykQe', 2, 2);

INSERT INTO movimiento(movimiento) VALUES('Creación de usuario');
INSERT INTO movimiento(movimiento) VALUES('Registro de usuario');
INSERT INTO movimiento(movimiento) VALUES('Cambio de estado de usuario');
INSERT INTO movimiento(movimiento) VALUES('Borrado de usuario');
INSERT INTO movimiento(movimiento) VALUES('Acceso al sistema');
INSERT INTO movimiento(movimiento) VALUES('Salida del sistema');
INSERT INTO movimiento(movimiento) VALUES('Cambio de contraseña');
INSERT INTO movimiento(movimiento) VALUES('Solicitud de cambio de contraseña');
