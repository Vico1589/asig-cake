--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

-- Started on 2019-01-15 13:22:48

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12278)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 199 (class 1259 OID 16669)
-- Name: estado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16642)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado (
    id integer DEFAULT nextval('public.estado_seq'::regclass) NOT NULL,
    estado character varying(20) NOT NULL
);


ALTER TABLE public.estado OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16672)
-- Name: tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_seq OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16647)
-- Name: tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo (
    id integer DEFAULT nextval('public.tipo_seq'::regclass) NOT NULL,
    tipo character varying(20) NOT NULL
);


ALTER TABLE public.tipo OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16675)
-- Name: usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_seq OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16652)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id integer DEFAULT nextval('public.usuario_seq'::regclass) NOT NULL,
    correo character varying(50) NOT NULL,
    apaterno character varying(50) NOT NULL,
    amaterno character varying(50),
    nombre character varying(50) NOT NULL,
    password character varying(60) NOT NULL,
    estado integer NOT NULL,
    tipo integer NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 2170 (class 0 OID 16642)
-- Dependencies: 196
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado (id, estado) FROM stdin;
1	activo
2	inactivo
\.


--
-- TOC entry 2171 (class 0 OID 16647)
-- Dependencies: 197
-- Data for Name: tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo (id, tipo) FROM stdin;
1	administrador
2	usuario
\.


--
-- TOC entry 2172 (class 0 OID 16652)
-- Dependencies: 198
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, correo, apaterno, amaterno, nombre, password, estado, tipo) FROM stdin;
2	user@correo.mx	User	User	User	$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa	1	2
1	admin@correo.mx	Admin	Admin	Admin	$2y$10$deNrNoxsKjZlPiseATGZquGbIzJtQtwvUQ57Dxn6DXCVge21LlIYa	1	1
39	nathand@uncharted.com	Drake		Nathan	$2y$10$rSwP1zj/hsf1f80pQlAYsOMMZHfFysWiWvtoWdpA22ZJkUpExi8JK	1	1
40	masterchief@halo.com	Master		Chief	$2y$10$OO76y/ixXLwNdacJu7ofY.u7gZShBg7VxHVCeTptUG/29xN0pOqJi	1	1
41	ezio_assassin@ubisoft.com	Auditore	da Firenze	Ezio	$2y$10$mEqzxlju.yAEl1pQObeer.1bLA00.Yjz/EOO9A/Oltv3yccyzIK3e	1	1
42	god_of_war@gow.com	NA		Kratos	$2y$10$OhiwIix9VbrYL5WTQEew4.wqGb7xiakjMlRbP/Jl8Q7tOVxIbAofe	1	1
43	andrew.bigd@bioshock.com	Ryan		Andrew	$2y$10$JFkkxZi5tBducYB334JZ9uWjsquGQH0E988uDpctFvUBxfGUu7BFO	1	1
44	jhonnybravo@gmail.com	Bravo	Castro	Juan Manuel	$2y$10$1yWT8/ahRH4iJSbYPQkj5.5VM/DzlDCs1lBt6ufYDmivQIvKwp0cG	1	2
45	geraltr@thewitcher.com	de Rivia		Geralt	$2y$10$jEtLu2kf6ExIFENMk0/KPePzFyKELIpMz4Wg7VLYL2CX7vbKWKsQ.	1	2
46	juliangm@gmail.com	García	Morales	Julián	$2y$10$raU8XtbTOz2cZ2FGDiHKXuFSf5Gwq0p0QPg.ZtYzlXJT23q6Oaq8u	1	2
47	fff@gmail.com	Fernández	Fernández	Fernando	$2y$10$fNE8FfzW6PR8dBqt.SxDBeD3iNvUaIZMku54DC1FUuQU9GX50ykQe	2	2
\.


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 199
-- Name: estado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_seq', 2, true);


--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 200
-- Name: tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_seq', 2, true);


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 201
-- Name: usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_seq', 50, true);


--
-- TOC entry 2040 (class 2606 OID 16646)
-- Name: estado pk_estado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT pk_estado PRIMARY KEY (id);


--
-- TOC entry 2042 (class 2606 OID 16651)
-- Name: tipo pk_tipo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo
    ADD CONSTRAINT pk_tipo PRIMARY KEY (id);


--
-- TOC entry 2044 (class 2606 OID 16656)
-- Name: usuario pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id);


--
-- TOC entry 2046 (class 2606 OID 16658)
-- Name: usuario usuario_correo_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_correo_key UNIQUE (correo);


--
-- TOC entry 2047 (class 2606 OID 16659)
-- Name: usuario fk_usuario_estado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk_usuario_estado FOREIGN KEY (estado) REFERENCES public.estado(id) MATCH FULL;


--
-- TOC entry 2048 (class 2606 OID 16664)
-- Name: usuario fk_usuario_tipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk_usuario_tipo FOREIGN KEY (tipo) REFERENCES public.tipo(id) MATCH FULL;


-- Completed on 2019-01-15 13:22:49

--
-- PostgreSQL database dump complete
--

