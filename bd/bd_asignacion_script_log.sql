
--CREATE DATABASE asigancion;

CREATE TABLE movimiento (
  id INT NOT NULL,
  movimiento VARCHAR(50) NOT NULL,
  CONSTRAINT pk_movimiento PRIMARY KEY (id)
);

CREATE TABLE registro(
  id INT NOT NULL,
  usuario INT NOT NULL,
  movimiento INT NOT NULL,
  fecha TIMESTAMPTZ,
  CONSTRAINT pk_registro PRIMARY KEY (id)
);

ALTER TABLE registro ADD CONSTRAINT fk_registro_usuario FOREIGN KEY (usuario)
REFERENCES usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE registro ADD CONSTRAINT fk_registro_movimiento FOREIGN KEY (movimiento)
REFERENCES movimiento (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE SEQUENCE movimiento_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE movimiento ALTER COLUMN id SET DEFAULT nextval('movimiento_seq');

CREATE SEQUENCE registro_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE registro ALTER COLUMN id SET DEFAULT nextval('registro_seq');


INSERT INTO movimiento(movimiento) VALUES('Creación de usuario');
INSERT INTO movimiento(movimiento) VALUES('Registro de usuario');
INSERT INTO movimiento(movimiento) VALUES('Cambio de estado de usuario');
INSERT INTO movimiento(movimiento) VALUES('Borrado de usuario');
INSERT INTO movimiento(movimiento) VALUES('Acceso al sistema');
INSERT INTO movimiento(movimiento) VALUES('Salida del sistema');
INSERT INTO movimiento(movimiento) VALUES('Cambio de contraseña');
INSERT INTO movimiento(movimiento) VALUES('Solicitud de cambio de contraseña');
