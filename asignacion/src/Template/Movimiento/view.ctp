<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Movimiento $movimiento
 */
?>
<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de movimientos'), ['action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Nuevo movimiento'), ['action' => 'add'], array('class' => 'btn btn-success text-white')); ?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Editar movimiento'), ['action' => 'edit', $movimiento->id], array('class' => 'btn btn-warning text-white')); ?> </a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Form->postLink(__('Borrar movimiento'), ['action' => 'delete', $movimiento->id], array('confirm' => __('¿Esta seguro que desea borrar el movimiento {0}?', $movimiento->movimiento), 'class'=>'btn btn-danger'))?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h3><?= h($movimiento->movimiento) ?></h3>
    <table class="table vertical-table table-responsive-lg table-striped">
        <tr>
            <th scope="row"><?= __('Movimiento') ?></th>
            <td><?= h($movimiento->movimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($movimiento->id) ?></td>
        </tr>
    </table>
</div>
