<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registro $registro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Registro'), ['action' => 'edit', $registro->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Registro'), ['action' => 'delete', $registro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registro->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Registro'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Registro'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="registro view large-9 medium-8 columns content">
    <h3><?= h($registro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($registro->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $this->Number->format($registro->usuario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Movimiento') ?></th>
            <td><?= $this->Number->format($registro->movimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($registro->fecha) ?></td>
        </tr>
    </table>
</div>
