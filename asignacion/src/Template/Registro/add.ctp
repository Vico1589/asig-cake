<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registro $registro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Registro'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="registro form large-9 medium-8 columns content">
    <?= $this->Form->create($registro) ?>
    <fieldset>
        <legend><?= __('Add Registro') ?></legend>
        <?php
            echo $this->Form->control('usuario');
            echo $this->Form->control('movimiento');
            echo $this->Form->control('fecha');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
