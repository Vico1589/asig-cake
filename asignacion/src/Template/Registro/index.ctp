<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registro[]|\Cake\Collection\CollectionInterface $registro
 */
?>

<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de usuarios'), ['controller'=>'Usuario', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de tipos'), ['controller'=>'Tipo', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de estados'), ['controller'=>'Estado', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de movimientos'), ['controller'=>'Movimiento', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <h3><?= __('Registro de movimientos') ?></h3>
     <table class="table table-responsive-lg table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', 'Id del movimiento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usuario', 'Usuario que realizo el movimiento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('correo', 'Correo del usuario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('movimiento', 'Movimiento realizado') ?></th>
                <!--th scope="col">< ?= $this->Paginator->sort('', 'Usuario afectado') ?></th-->
                <th scope="col"><?= $this->Paginator->sort('fecha', 'Fecha del movimiento') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($registro as $registro): ?>
            <tr>
                <td><?= $this->Number->format($registro->id) ?></td>
                <td><?= h($registro['Usuario']['nombre']." ".$registro['Usuario']['apaterno']) ?></td>
                <td><?= h($registro['Usuario']['correo']) ?> </td>
                <td><?= h($registro['Movimiento']['movimiento']) ?></td>
                <!--td>< ?= h($registro['Movimiento']['movimiento']) ?></td-->
                <td><?= h($registro->fecha) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Inicio')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
