<head>
    <script src='https://www.google.com/recaptcha/api.js?render=6LdBtIoUAAAAAEuNv1WzxX6reWaLU7BEBFEu9EyU'></script>
</head>
<body>
    <div class="container">
        <h2>Acceso</h2>
        <?= $this->Form->create() ?>
        <?= $this->Form->control('correo') ?>
        <?= $this->Form->control('password') ?>
        <!--?= $this->Form->input('correo', array('type'=>'email', 'label'=>'Correo')) ?-->
        <!--?= $this->Form->input('password', array('type'=>'password', 'label'=>'Contraseña')) ?-->
        <?= $this->Recaptcha->display() ?> 
        <?= $this->Form->button('Acceder', array('class' => 'offset-4 col-4 btn btn-success center')) ?>
        <?= $this->Form->end() ?>
    </div>
    
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('reCAPTCHA_site_key', {action: 'homepage'}).then(function(token) {
               ...
            });
        });
    </script>
</body>

