<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Cambio de contraseña') ?></legend>
        <?=  $this->Form->input('passwordAnt', array('label' => 'Contraseña actual', 'type' => 'password')); ?>
        <?=  $this->Form->input('passwordNue', array('label' => 'Nueva contraseña', 'type' => 'password')); ?>
        <?= $this->Form->input('passwordNue2', array('label' => 'Confirma la nueva contraseña', 'type' => 'password')); ?>
    </fieldset><br>
        <?= $this->Form->button(__('Guardar'), array('class' => 'col-5 btn btn-success left')) ?>
        <?= $this->Form->button(__('Cancelar'), array('type' => 'button', 'onclick' => 'location.href=\'/asignacion/usuario/home\'', 'class'=>'col-5 btn btn-danger right')) ?>
        <?= $this->Form->end() ?>
</div>

