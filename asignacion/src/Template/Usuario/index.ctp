<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario[]|\Cake\Collection\CollectionInterface $usuario
 */
?>

<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de tipos'), ['controller'=>'Tipo', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de estados'), ['controller'=>'Estado', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de movimientos'), ['controller'=>'Movimiento', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Registro de movimientos'), ['controller'=>'Registro', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Nuevo usuario'), ['action' => 'add'], array('class' => 'btn btn-success text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <h3><?= __('Catálogo de usuarios') ?></h3>
    <table class="table table-responsive-lg table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('apaterno', 'Apellido Paterno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amaterno', 'Apellido Materno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre', 'Nombre(s)') ?></th>
                <th scope="col"><?= $this->Paginator->sort('correo', 'Correo') ?></th>
                <th scope="col"><?= __('Tipo') ?></th>
                <th scope="col"><?= __('Estado') ?></th>
                <th scope="col" class="actions"><?= __('Opciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuario as $usuario): ?>
            <tr>
                <td><?= h($usuario->apaterno) ?></td>
                <td><?= h($usuario->amaterno) ?></td>
                <td><?= h($usuario->nombre) ?></td>
                <td><?= h($usuario->correo) ?></td>
                <td><?= h($usuario['Tipo']['tipo']) ?></td>
                <td><?= h($usuario['Estado']['estado']) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $usuario->id], ['class'=>'btn btn-info']) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $usuario->id], ['class'=>'btn btn-warning']) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $usuario->id], array('confirm' => __('¿Esta seguro que desea borrar al usuario {0}?', $usuario->correo), 'class'=>'btn btn-danger'))?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Inicio')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
