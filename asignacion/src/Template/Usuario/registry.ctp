<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>

<div class="container">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Nuevo Usuario') ?></legend>
        <?php
            echo $this->Form->input('correo', array('type'=>'email', 'label' => 'Correo'));
            echo $this->Form->input('apaterno', array('label' => 'Apellido Paterno'));
            echo $this->Form->input('amaterno', array('label' => 'Apellido Materno'));
            echo $this->Form->input('nombre', array('label' => 'Nombre(s)'));
            echo $this->Form->hidden('password', array('type'=>'password', 'value'=>$usuario->passwordB));
            echo $this->Form->hidden('estado', array('value'=>1));
            echo $this->Form->hidden('tipo', array( 'value' =>2));        
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), array('class' => 'col-5 btn btn-success')) ?>
    <?= $this->Form->button(__('Cancelar'), array('type' => 'button', 'onclick' => 'location.href=\'/asignacion/users/login\'', 'class'=>'col-5 btn btn-danger right')) ?>
    <?= $this->Form->end() ?>
</div>
