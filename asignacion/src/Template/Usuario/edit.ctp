<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>

<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de usuarios'), ['action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Form->postLink(__('Borrar usuario'), ['action' => 'delete', $usuario->id], array('confirm' => __('¿Esta seguro que desea borrar al usuario {0}?', $usuario->correo), 'class'=>'btn btn-danger'))?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <?= $this->Form->create($usuario, ['type'=>'file']) ?>
    <fieldset>
        <legend><?= __('Editar usuario {0}', $usuario->correo) ?></legend>
        <?php
            echo $this->Form->hidden('correo');
            echo $this->Form->input('apaterno', array('label' => 'Apellido Paterno'));
            echo $this->Form->input('amaterno', array('label' => 'Apellido Materno'));
            echo $this->Form->input('nombre', array('label' => 'Nombre(s)'));
            echo $this->Form->input('password', array('type'=>'password', 'label' => 'Contraseña'));
            echo $this->Html->image('/webroot/files/usuario/photo/'. $usuario->photo_dir.'/'.'square_'. $usuario->photo,['style'=>'width: 100px; height: 100px; margin-bottom: 20px;', 'class'=>'img-thumbnail rounded']);
            echo $this->Form->input('photo', array('type' => 'file', 'label' => 'Imagen de perfil'));
            echo $this->Form->input('estado', array('type'=>'select', 'name'=>'estado', 'label' =>['text'=>'Estado', 'class'=>'col-1 float-left'], 'options'=>['Elige una opcíon', 'Activo', 'Inactivo'], 'disabled' => ['0'], 'class'=>'col-4 float-left'));
            echo $this->Form->input('tipo', array('type'=>'select', 'label'=>['text'=>'Tipo', 'class'=>'col-1 float-left offset-1'], 'options'=>['Elige una opción', 'Administrador', 'Usuario'], 'disabled' => ['0'], 'class'=>'col-4 float-left'));
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), array('class' => 'col-5 btn btn-success left')) ?>
    <?= $this->Form->button(__('Cancelar'), array('type' => 'button', 'onclick' => 'location.href=\'/asignacion/usuario\'', 'class'=>'col-5 btn btn-danger right')) ?>
    <?= $this->Form->end() ?>
</div>
