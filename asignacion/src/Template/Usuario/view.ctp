<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>

<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de usuarios'), ['action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Nuevo usuario'), ['action' => 'add'], array('class' => 'btn btn-success text-white')); ?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Editar usuario'), ['action' => 'edit', $usuario->id], array('class' => 'btn btn-warning text-white')); ?> </a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Form->postLink(__('Borrar usuario'), ['action' => 'delete', $usuario->id], array('confirm' => __('¿Esta seguro que desea borrar al usuario {0}?', $usuario->correo), 'class'=>'btn btn-danger'))?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h3><?= h($usuario->nombre)." ".h($usuario->apaterno)  ?></h3>
    <div>
        <div><?= $this->Html->image('/webroot/files/usuario/photo/'. $usuario->photo_dir.'/'.'square_'. $usuario->photo,['style'=>'width: 200px; height: 200px; margin-bottom: 20px;', 'class'=>'img-thumbnail rounded float-left']); ?></div>
    </div>
    <table class="table vertical-table table-responsive-lg table-striped">
        <tr>
            <th scope="row"><?= __('Correo') ?></th>
            <td><?= h($usuario->correo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Paterno') ?></th>
            <td><?= h($usuario->apaterno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Materno') ?></th>
            <td><?= h($usuario->amaterno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre(s)') ?></th>
            <td><?= h($usuario->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <td><?= h($usuario['Tipo']['tipo']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= h($usuario['Estado']['estado']) ?></td>
        </tr>
    </table>
</div>
