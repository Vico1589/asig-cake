<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cambiar contraseña'), ['action' => 'changePassword', $usuario->read('Auth.User.id')], array('class'=>'btn btn-warning text-white'))?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <h3><?= h($usuario->read('Auth.User.nombre'))." ".h($usuario->read('Auth.User.apaterno'))  ?></h3>
    <div>
        <div><?= $this->Html->image('/webroot/files/usuario/photo/'. $usuario->read('Auth.User.photo_dir').'/'.'square_'. $usuario->read('Auth.User.photo'),['style'=>'width: 200px; height: 200px; margin-bottom: 20px;', 'class'=>'img-thumbnail rounded float-left']); ?></div>
    </div>
    <table class="table vertical-table table-responsive-lg table-striped">
        <tr>
            <th scope="row"><?= __('Correo') ?></th>
            <td><?= h($usuario->read('Auth.User.correo')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre(s)') ?></th>
            <td><?= h($usuario->read('Auth.User.nombre')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Paterno') ?></th>
            <td><?= h($usuario->read('Auth.User.apaterno')) ?></td>
        </tr>
    </table>
</div>

