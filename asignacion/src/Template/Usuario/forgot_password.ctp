<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>

<div class="container">
   <?= $this->Form->create($usuario); ?>
    <fieldset>
        <legend><?= __('Recuperar contraseña') ?></legend>
        <?= $this->Form->input('correo', array('type'=>'email', 'label'=>'Correo')) ?>
        <div class="offset-4 col-4" style="margin-bottom: 30px; padding-left: 50px;"><?= $this->Recaptcha->display() ?></div>
    </fieldset>
    <?= $this->Form->button(__('Enviar'), array('class' => 'col-5 btn btn-success left')) ?>
    <?= $this->Form->button(__('Cancelar'), array('type' => 'button', 'onclick' => 'location.href=\'/asignacion/users/login\'', 'class'=>'col-5 btn btn-danger right')) ?>
    <?= $this->Form->end() ?>
</div>

