<!--<head>
    <script src='https://www.google.com/recaptcha/api.js?render=6LdBtIoUAAAAAEuNv1WzxX6reWaLU7BEBFEu9EyU'></script>
</head>-->
<body>
    <div class="container">
        <h2>Acceso</h2>
        <?= $this->Form->create() ?>
        <!--?= $this->Form->control('correo') ?-->
        <!--?= $this->Form->control('password') ?-->
        <?= $this->Form->input('correo', array('type'=>'email', 'label'=>'Correo')) ?>
        <?= $this->Form->input('password', array('type'=>'password', 'label'=>'Contraseña')) ?>
        <div class="offset-4 col-4" style="margin-bottom: 30px; padding-left: 50px;"><?= $this->Recaptcha->display() ?></div>
        <?= $this->Form->button('Acceder', array('class' => 'offset-3 col-6 btn btn-success center')) ?>
        <div class="row">
            <?= $this->Form->button(__('Registrarse'), array('type' => 'button', 'onclick' => 'location.href=\'/asignacion/usuario/registry\'', 'class'=>'col-5 btn btn-sm btn-info')) ?>
            <?= $this->Form->button(__('¿Olvidaste tu contraseña?'), array('type' => 'button', 'onclick' => 'location.href=\'/asignacion/usuario/forgotPassword\'', 'class'=>'col-5 btn btn-sm offset-2 btn-warning')) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
    
<!--    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('reCAPTCHA_site_key', {action: 'homepage'}).then(function(token) {
               ...
            });
        });
    </script>-->

</body>

