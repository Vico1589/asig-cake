<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CRUD Usuarios';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
    <?php echo $this->Html->css('bootstrap.min.css') ?>
    <?php echo $this->Html->script('jquery-3.3.1.min.js') ?>
    <?php echo $this->Html->script('popper.min.js') ?>
    <?php echo $this->Html->script('bootstrap.min.js') ?>
    
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,0,0.5) ; margin-bottom: 20px;">
        <?= $this->Html->image('../img/icono-usuario.png', ['style' => 'height: 90px;', 'style' => 'width: 90px;']); ?>
        <ul> 
            <h2 style="color: white;"><?= __('Administración de usuarios') ?></h2>
        </ul>
    </nav>
    
    
    <div class="container-fluid">
        <?= $this->Flash->render() ?>
        <div class="container-fluid">
            <?= $this->fetch('content') ?>
        </div>
    </div>
    
    <footer>
    </footer>
</body>
</html>
