<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <title>Bienvenido</title>
</head>
<body>
    <div class="container">
        <h3 class="text-center">Hola, <?= $nombre . " " . $apaterno ?></h3><br>
        <div class="card" style="width: 18rem;">
          <div class="card-header">
            Has solicitado un cambio de contraseña. A continuación se muestra la nueva información de acceso.
          </div>
          <br>
          <ul class="list-group list-group-flush">
              Información de acceso
            <li class="list-group-item">Correo: <h4><?= $correo?></h4></li>
            <li class="list-group-item">Contraseña: <h4><?= $password?></h4></li>
          </ul>
        </div>
       
        <p>Le recomendamos iniciar sesión y cambiar la contraseña por defecto.</p>
        <p>Atentamente</p>
        <p>El equipo de desarrollo</p>
    </div>
    <div>     
        <p>Si usted no ha solicitado el cambio de contraseña, omita este correo.</p>
    </div>
    <footer class="centered-text">
        <p>Este correo es informativo y se envía de forma automatizada, favor no responder a esta dirección de correo.</p>
        <p>Si requiere mayor información sobre el contenido de este mensaje, contactar a Víctor Ruiz en horario de
        lunes a viernes de 9:00 a 13:00 y de 14:00 a 18:00 horas, teléfono XXXX-XXXX o al e-mail victor.mra89@gmail.com</p>
    </footer>
</body>