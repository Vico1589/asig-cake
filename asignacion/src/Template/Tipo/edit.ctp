<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tipo $tipo
 */
?>

<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de tipos'), ['action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Form->postLink(__('Borrar tipo'), ['action' => 'delete', $tipo->id], array('confirm' => __('¿Esta seguro que desea borrar el tipo {0}?', $tipo->tipo), 'class'=>'btn btn-danger'))?></a>
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample3">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <?= $this->Form->create($tipo) ?>
    <fieldset>
        <legend><?= __('Editar Tipo de Usuario') ?></legend>
        <?php
            echo $this->Form->input('tipo', array('label' => 'Nombre del tipo de usuario'));
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), array('class' => 'btn btn-success right')) ?>
    <?= $this->Form->end() ?>
</div>
