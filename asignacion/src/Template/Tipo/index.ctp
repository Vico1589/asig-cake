<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tipo[]|\Cake\Collection\CollectionInterface $tipo
 */
?>

<div class="container-fluid" style="margin-bottom: 15px;">
    <nav class="navbar-dark">
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2" style="background: rgba(0, 0, 0, 0.5);">
            <span class="navbar-toggler-icon"></span> Menú
        </button>
    </nav>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de usuarios'), ['controller'=>'Usuario', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de estados'), ['controller'=>'Estado', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Catálogo de movimientos'), ['controller'=>'Movimiento', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Registro de movimientos'), ['controller'=>'Registro', 'action' => 'index'], array('class' => 'btn btn-info text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Nuevo tipo'), ['action' => 'add'], array('class' => 'btn btn-success text-white')); ?></a>      
            </div>
        </div>
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
                <a class="nav-link" href="javascript:void(0)"><?= $this->Html->link(__('Cerrar sesión'), ['controller'=>'Users', 'action' => 'logout'], array('class' => 'btn btn-danger text-white')); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h3><?= __('Catálogo de tipos de usuario') ?></h3>
    <table class="table table-responsive-lg table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', 'Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipo', 'Tipo') ?></th>
                <th scope="col" class="actions"><?= __('Opciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipo as $tipo): ?>
            <tr>
                <td><?= $this->Number->format($tipo->id) ?></td>
                <td><?= h($tipo->tipo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $tipo->id], ['class'=>'btn btn-info']) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tipo->id], ['class'=>'btn btn-warning']) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $tipo->id], array('confirm' => __('¿Esta seguro que desea borrar el tipo {0}?', $tipo->tipo), 'class'=>'btn btn-danger'))?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Inicio')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
