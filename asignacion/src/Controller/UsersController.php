<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class UsersController extends AppController{
    
    public function beforeFilter(Event $event){ 
        parent::beforeFilter($event); 
    }
    
    public function isAuthorized($usuario){
        
        if ($usuario['tipo'] == 2) {
            $allowedActions = ['home', 'logout'];
            if (in_array($this->request->action, $allowedActions)) { 
                return true; 
            }
        }
        
        if ($usuario['tipo'] == 1) { 
            return true; 
        }
    }

    public function login(){
        if($this->Auth->user()){
            $this->Flash->error(__('Ya has iniciado sesión'));
            return $this->redirect('/usuario/home');
        }

        if ($this->request->is('post')){ 
            if ($this->Recaptcha->verify()) {
            $usuario = $this->Auth->identify();
                if ($usuario) {
                    $this->Auth->setUser($usuario);
                        
                    $this->registrarBitacora(5, $usuario['id']);

                    if (($usuario['tipo'] == 1) && ($usuario['estado'] == 1)) {    
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                    else if (($usuario['tipo'] == 2) && ($usuario['estado'] == 1)){
                        return $this->redirect(['controller' => 'Usuario', 'action' => 'home']);
                    }
                    else{
                       $this->Flash->error('El usuario esta inactivo.'); 
                       $this->registrarBitacora(6, $this->getRequest()->getSession()->read('Auth.User.id'));
                       return $this->redirect($this->Auth->logout()); 
                    }
                }
                $this->Flash->error('El correo o la contraseña ingresada no son correctos. Intenta de nuevo.');
            }
            $this->Flash->error('El captcha no fue resuelto. Intenta de nuevo.');
        }
    }

    public function logout(){   
        
        $this->registrarBitacora(6, $this->getRequest()->getSession()->read('Auth.User.id'));

        $this->Flash->success('Hasta luego.');
        return $this->redirect($this->Auth->logout());
    }
    
    public function registrarBitacora($movimiento, $usuario){
        $registro = TableRegistry::get('Registro');
        $query = $registro->query();
        $query->insert(['usuario', 'movimiento', 'fecha'])
                ->values([
                    'usuario' => $usuario,
                    'movimiento' => $movimiento,
                    'fecha'=>'Now()'])
                ->execute();
    }
    
}