<?php
namespace App\Controller;

use App\Controller\AppController;

class RegistroController extends AppController{
     public $paginate = [
        'limit' => 10,
        'order' => [
            'Registro.id' => 'asc'
        ]
    ];
    
    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function isAuthorized($usuario){
    
        if ($usuario['tipo'] == 1) { 
            return true; 
        }else{
            return false;
        }
    }

    /**
     * Index
     */
    public function index(){
        
        $registro = $this->Registro->find()->select(['id', 'Usuario.nombre', 'Usuario.apaterno', 'Usuario.correo', 'Movimiento.movimiento', 'fecha'])
            ->join(['table' => 'usuario', 'alias' => 'Usuario', 'type' => 'INNER', 'conditions' => 'Registro.usuario = Usuario.id'])
            ->join(['table' => 'movimiento', 'alias' => 'Movimiento', 'type' => 'INNER', 'conditions' => 'Registro.movimiento = Movimiento.id']);
        $registro = $this->paginate($registro);
        $this->set(compact('registro'));
    }
    
}
