<?php

namespace App\Controller;

use App\Controller\AppController;
//use Cake\Network\Email\Email;
use Cake\Mailer\Email;

class CorreoController extends AppController{
    public function correo($usuario, $passRand){
        $this->autoRender = false;

            //Opciones para conectar al servidor smtp de Gmail

//        Email::configTransport('mail', [
//            'host' => 'ssl://smtp.gmail.com', //servidor smtp con encriptacion ssl
//            'port' => 465, //puerto de conexion
//            //'tls' => true, //true en caso de usar encriptacion tls
//
//            //configuracion de cuenta de correo
//            'username' => 'becario.victor.ruiz@gmail.com', 
//            'password' => 'Hola1234', //contrasena
//
//            //Establecemos que vamos a utilizar el envio de correo por smtp
//            'className' => 'Smtp', 

            //evitar verificacion de certificado ssl ---IMPORTANTE---
            /*'context' => [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ]
            ]*/
//        ]); 
            /*fin configuracion de smtp*/

            /*enviando el correo*/
        $correo = new Email(); //instancia de correo
        $correo   
            ->transport('gmail') //nombre del configTrasnport que acabamos de configurar
            ->template('correo_plantilla') //plantilla a utilizar
            ->emailFormat('html') //formato de correo
            ->to($usuario->email) //correo para
            ->from('becario.victor.ruiz@gmail.com') //correo de
            ->subject('Bienvenido a bordo') //asunto
            ->viewVars([
                'nombre' => $usuario->nombre, 
                'apaterno' => $usuario->apaterno, 
                'email' => $usuario->correo, 
                'password' => $passRand
            ]);
            
            if($correo->send()){
              echo "Correo enviado.";
              return $this->redirect(['action' => 'index']);
            }else{
              echo "No fue posible enviar el correo electrónico.";
            }    
    }
}

