<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Tipo Controller
 *
 * @property \App\Model\Table\TipoTable $Tipo
 *
 * @method \App\Model\Entity\Tipo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TipoController extends AppController {
    
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Tipo.id' => 'asc'
        ]
    ];
    
    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function isAuthorized($usuario){
        
        if ($usuario['tipo'] == 1) { 
            return true; 
        } else {
            return false;
        }
    }
    
    /**
     * Index
     */
    public function index() {
        $tipo = $this->paginate($this->Tipo);

        $this->set(compact('tipo'));
    }

    /**
     * View 
     */
    public function view($id = null) {
        $tipo = $this->Tipo->get($id, [
            'contain' => []
        ]);

        $this->set('tipo', $tipo);
    }

    /**
     * Add 
     */
    public function add() {
        $tipo = $this->Tipo->newEntity();
        if ($this->request->is('post')) {
            $tipo = $this->Tipo->patchEntity($tipo, $this->request->getData());
            if ($this->Tipo->save($tipo)) {
                $this->Flash->success(__('The tipo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tipo could not be saved. Please, try again.'));
        }
        $this->set(compact('tipo'));
    }

    /**
     * Edit
     */
    public function edit($id = null) {
        $tipo = $this->Tipo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipo = $this->Tipo->patchEntity($tipo, $this->request->getData());
            if ($this->Tipo->save($tipo)) {
                $this->Flash->success(__('The tipo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tipo could not be saved. Please, try again.'));
        }
        $this->set(compact('tipo'));
    }
    
    /**
     * Delete 
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tipo = $this->Tipo->get($id);
        if ($this->Tipo->delete($tipo)) {
            $this->Flash->success(__('The tipo has been deleted.'));
        } else {
            $this->Flash->error(__('The tipo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
}
