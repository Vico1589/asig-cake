<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Movimiento Controller
 *
 * @property \App\Model\Table\MovimientoTable $Movimiento
 *
 * @method \App\Model\Entity\Movimiento[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MovimientoController extends AppController{
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Movimiento.id' => 'asc'
        ]
    ];
    
    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function isAuthorized($usuario){
        if ($usuario['tipo'] == 1) { 
            return true; 
        }else{
            return false;
        }
    }
    
    /**
     * Index
     */
    public function index(){
        $movimiento = $this->paginate($this->Movimiento);

        $this->set(compact('movimiento'));
    }

    /**
     * View
     */
    public function view($id = null){
        $movimiento = $this->Movimiento->get($id, [
            'contain' => []
        ]);

        $this->set('movimiento', $movimiento);
    }

    /**
     * Add 
     */
    public function add(){
        $movimiento = $this->Movimiento->newEntity();
        if ($this->request->is('post')) {
            $movimiento = $this->Movimiento->patchEntity($movimiento, $this->request->getData());
            if ($this->Movimiento->save($movimiento)) {
                $this->Flash->success(__('The movimiento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The movimiento could not be saved. Please, try again.'));
        }
        $this->set(compact('movimiento'));
    }

    /**
     * Edit
     */
    public function edit($id = null){
        $movimiento = $this->Movimiento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movimiento = $this->Movimiento->patchEntity($movimiento, $this->request->getData());
            if ($this->Movimiento->save($movimiento)) {
                $this->Flash->success(__('The movimiento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The movimiento could not be saved. Please, try again.'));
        }
        $this->set(compact('movimiento'));
    }

    /**
     * Delete
     */
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $movimiento = $this->Movimiento->get($id);
        if ($this->Movimiento->delete($movimiento)) {
            $this->Flash->success(__('The movimiento has been deleted.'));
        } else {
            $this->Flash->error(__('The movimiento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
}
