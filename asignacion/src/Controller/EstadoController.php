<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Estado Controller
 *
 * @property \App\Model\Table\EstadoTable $Estado
 *
 * @method \App\Model\Entity\Estado[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EstadoController extends AppController{

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Estado.id' => 'asc'
        ]
    ];
    
    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function isAuthorized($usuario){
        if ($usuario['tipo'] == 1) { 
            return true; 
        }else{
            return false;
        }
    }
    
    /**
     * Index 
     */
    public function index(){
        $estado = $this->paginate($this->Estado);

        $this->set(compact('estado'));
    }

    /**
     * View 
     */
    public function view($id = null){
        $estado = $this->Estado->get($id, [
            'contain' => []
        ]);

        $this->set('estado', $estado);
    }

    /**
     * Add
     */
    public function add(){
        $estado = $this->Estado->newEntity();
        if ($this->request->is('post')) {
            $estado = $this->Estado->patchEntity($estado, $this->request->getData());
            if ($this->Estado->save($estado)) {
                $this->Flash->success(__('The estado has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The estado could not be saved. Please, try again.'));
        }
        $this->set(compact('estado'));
    }

    /**
     * Edit 
     */
    public function edit($id = null){
        $estado = $this->Estado->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estado = $this->Estado->patchEntity($estado, $this->request->getData());
            if ($this->Estado->save($estado)) {
                $this->Flash->success(__('The estado has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The estado could not be saved. Please, try again.'));
        }
        $this->set(compact('estado'));
    }

    /**
     * Delete method
     */
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $estado = $this->Estado->get($id);
        if ($this->Estado->delete($estado)) {
            $this->Flash->success(__('The estado has been deleted.'));
        } else {
            $this->Flash->error(__('The estado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
}
