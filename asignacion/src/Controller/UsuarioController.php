<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class UsuarioController extends AppController{
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Usuario.apaterno' => 'asc'
        ]
    ];

    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event){ 
        parent::beforeFilter($event);      
    }
    
    public function isAuthorized($usuario){
        
        if ($usuario['tipo'] == 2) {
            $allowedActions = ['home', 'logout', 'changePassword'];
            if (in_array($this->request->action, $allowedActions)) { 
                return true; 
            }
        }
        
        if ($usuario['tipo'] == 1) { 
            return true; 
        }
    }
    
    /**
     * Index
     */
    public function index(){
        $usuario = $this->Usuario->find()->select(['id', 'apaterno', 'amaterno', 'nombre', 'correo', 'password', 'Estado.estado', 'Tipo.tipo'])
            ->join(['table' => 'tipo', 'alias' => 'Tipo', 'type' => 'INNER', 'conditions' => 'Usuario.tipo = Tipo.id'])
            ->join(['table' => 'estado', 'alias' => 'Estado', 'type' => 'INNER', 'conditions' => 'Usuario.estado = Estado.id']);
        $usuario = $this->paginate($usuario);
        $this->set(compact('usuario'));
    }

    /**
     * Ver
     */
    public function view($id = null){
        $usuario= $this->Usuario->find()->select(['id', 'apaterno', 'amaterno',  'nombre', 'correo', 'photo', 'photo_dir', 'Estado.estado', 'Tipo.tipo'])
            ->join(['table' => 'tipo', 'alias' => 'Tipo', 'type' => 'INNER', 'conditions' => 'Usuario.tipo = Tipo.id'])
            ->join(['table' => 'estado', 'alias' => 'Estado', 'type' => 'INNER', 'conditions' => 'Usuario.estado = Estado.id'])
            ->where(['Usuario.id' => $id])
            ->first();
        $this->set(compact('usuario'));       
    }

    /**
     * Añadir
     */
     
    public function generateRandomString($length) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-!#/()=?', ceil($length/strlen($x)) )),1,$length);
    }
    
    public function add(){
        $usuario = $this->Usuario->newEntity();
        $passRand = $this->generateRandomString(8);
        $usuario->passwordB = $passRand;
        if ($this->request->is('post')) {
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData());
            if ($this->Usuario->save($usuario)) {
                $this->Flash->success(__('El usuario ha sido registrado exitosamente. Pronto recibirá un correo electrónico con la contraseña.')); 
                
                $this->registrarBitacora(1, $this->getRequest()->getSession()->read('Auth.User.id'));
                $this->enviarCorreo($usuario, $passRand, 'welcome', 'Bienvenido a bordo');
                    return $this->redirect(['action' => 'index']);                   
            }
            $this->Flash->error(__('El usuario no ha sido guardado. Inente de nuevo.'));
        }
        $this->set(compact('usuario'));
    }
    
    public function registry(){
        $usuario = $this->Usuario->newEntity();
        $passRand = $this->generateRandomString(8);
        $usuario->passwordB = $passRand;
        if ($this->request->is('post')) {
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData());
            if ($this->Usuario->save($usuario)) {
                $this->Flash->success(__('El usuario ha sido registrado exitosamente. Pronto recibirá un correo electrónico con la contraseña.')); 
                
                $this->registrarBitacora(3, $usuario->id);
                $this->enviarCorreo($usuario, $passRand, 'welcome', 'Bienvenido a bordo');
                        
                    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
        $this->Flash->error('El captcha no fue resuelto. Intenta de nuevo.');
        }
        $this->set(compact('usuario'));
    }

    /**
     * Editar
     */
    public function edit($id = null){
        $usuario = $this->Usuario->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData());
            if ($this->Usuario->save($usuario)) {
                $this->Flash->success(__('El usuario ha sido modificado.'));
                
                $this->registrarBitacora(2, $this->getRequest()->getSession()->read('Auth.User.id'));
                
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no ha sido modificado. Inente de nuevo.'));
        }
        $this->set(compact('usuario'));
    }

    /**
     * Eliminar
     */
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $usuario = $this->Usuario->get($id);
        if ($this->Usuario->delete($usuario)) {
            $this->Flash->success(__('El usuario ha sido borrado.'));
            
            $this->registrarBitacora(4, $this->getRequest()->getSession()->read('Auth.User.id'));
        } else {
            $this->Flash->error(__('El usuario no pudo ser borrado. Intente de nuevo.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function home(){
        $usuario = $this->getRequest()->getSession();
        $this->set(compact('usuario'));
    }
    
    public function changePassword($id) {
        $usuario = $this->Usuario->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData());
            $usuario->password = $this->request->data['passwordNue'];
            if ($this->Usuario->save($usuario)) {
                $this->registrarBitacora(7, $this->getRequest()->getSession()->read('Auth.User.id'));
                $this->Flash->success(__('Se ha cambiado la contraseña'));
                ($this->getRequest()->getSession()->read('Auth.User.tipo') === 1) ? $this->redirect(['action' => 'index']) : $this->redirect(['action' => 'home']);
            } else {
                $this->Flash->error(__('No se ha cambiado la contraseña. Intenta de nuevo.'));
            }
        }
        $this->set(compact('usuario'));

    }
    
    public function forgotPassword() {
        $usuario = $this->Usuario->newEntity();
    if ($this->request->is(['patch', 'post', 'put'])){
        if ($this->Recaptcha->verify()){
            $passRand = $this->generateRandomString(8);
            $usuario = $this->Usuario->findByCorreo($this->request->data['correo'])->first();
            if (empty($usuario)) {
                $this->Flash->error('La dirección de correo no se encuantra registrada. Intenta de nuevo.');
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            } 
            $this->Usuario->patchEntity($usuario, $this->request->getData());
            $usuario->password = $passRand;
                
                if ($this->Usuario->save($usuario)) {
                    
                    $this->Flash->success(__('La nueva contraseña ha sido enviada al correo registrado.'));
                    
                    $this->registrarBitacora(8, $usuario['id']);
                    $this->enviarCorreo($usuario, $passRand, 'forgot_password', 'Solicitud de cambio de contraseña');
                    
                    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
                }
        }
    }
        $this->set(compact('usuario'));    
    }      
    
    public function enviarCorreo($usuario, $passRand, $plantilla = "", $subject = ""){
        $correo = new Email();
        $correo->transport('default')
               ->template($plantilla)
               ->emailFormat('html')
               ->to($usuario->correo)
               ->subject($subject)
               ->viewVars(['nombre' => $usuario->nombre,
                           'apaterno' => $usuario->apaterno,
                           'amaterno' => $usuario->amaterno,
                           'correo' => $usuario->correo,
                           'password' => $passRand]);
        if($correo->send()){
            $this->Flash->success(__('La contraseña ha sido enviada al nuevo correo registrado'));
        } else {
            $this->Flash->success(__('Error al envíar correo con contraseña del usuario.'));
        }
    }
    
    public function registrarBitacora($movimiento, $usuario){
        $registro = TableRegistry::get('Registro');
        $query = $registro->query();
        $query->insert(['usuario', 'movimiento', 'fecha'])
                ->values([
                    'usuario' => $usuario,
                    'movimiento' => $movimiento,
                    'fecha'=>'Now()'])
                ->execute();
    }
}


