<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller{

    public function initialize(){
        parent::initialize();

        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,
            'sitekey' => '6Lcg6IsUAAAAACrdGhOUxIDptagHSNLPvAN2hNXW',
            'secret' => '6Lcg6IsUAAAAAAF032WrWMTVtZ8MUkmTuO9bDuys',
            'type' => 'image', 
            'theme' => 'light',
            'lang' => 'es',
            'size' => 'normal'
        ]);

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'correo', 'correo',
                        'password' => 'password'
                    ], 'userModel' => 'usuario',                   
                ],            
            ], 
            'loginRedirect' => [
                'controller' => 'Usuario',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => 'Session',
            'authError' => 'No tienes permisos para acceder a este sitio.',
            'unauthorizedRedirect' => ['controller' => 'Users', 'action' => 'home']
        ]);
        
    }
    
    public function beforeFilter(Event $event){
        $this->Auth->allow(['registry', 'login', 'forgotPassword']);
    }

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
}
