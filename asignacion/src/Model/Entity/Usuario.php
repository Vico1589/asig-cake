<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class Usuario extends Entity
{
    protected $_accessible = [
        'correo' => true,
        'apaterno' => true,
        'amaterno' => true,
        'nombre' => true,
        'password' => true,
        'estado' => true,
        'tipo' => true,
        'photo' => true,
        'photo_dir' => false,
    ];

    protected $_hidden = [
        'password'
    ];
    
    protected function _setPassword($password){
        if (strlen($password)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($password);
        }
    }
    
}
