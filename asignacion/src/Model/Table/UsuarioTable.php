<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class UsuarioTable extends Table{
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('usuario');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->addBehavior('Proffer.Proffer', [
            'photo' => [	// The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'photo_dir',	// The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [	// Define the prefix of your thumbnail
                        'w' => 200,	// Width
                        'h' => 200,	// Height
                        'jpeg_quality'	=> 100
                    ],
                ],
                'thumbnailMethod' => 'gd'	// Options are Imagick or Gd
            ]
        ]);
        
        //$this->hasOne('tipo');
        //$this->hasOne('estado');
    }

    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->email('correo')
            ->maxLength('correo', 50, 'El correo sólo puede tener 50 caracteres.')
            ->requirePresence('correo', 'create')
            ->allowEmptyString('correo', false)
            ->add('correo', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'El correo ya se encuentra registrado.']);
        
        $validator
            ->scalar('apaterno')
            ->maxLength('apaterno', 50)
            ->requirePresence('apaterno', 'create')
            ->allowEmptyString('apaterno', false)
            ->add('apaterno', 'validFormat', [
                'rule' => array('custom', '/[A-Za-zÁÉÍÓÚáéíóúÑñ ]$/i'),
                'message' => 'El valor ingresado no es válido']);

        $validator
            ->scalar('amaterno')
            ->maxLength('amaterno', 50)
            ->allowEmptyString('amaterno')
            ->add('amaterno', 'validFormat', [
                'rule' => array('custom', '/[A-Za-zÁÉÍÓÚáéíóúÑñ ]$/i'),
                'message' => 'El valor ingresado no es válido']);

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 50)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false)
            ->add('nombre', 'validFormat', [
                'rule' => array('custom', '/[A-Za-zÁÉÍÓÚáéíóúÑñ ]$/i'),
                'message' => 'El valor ingresado no es válido']);

        $validator
            ->scalar('password')
            ->maxLength('password', 60)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);

        $validator
            ->integer('estado')
            ->requirePresence('estado', 'create')
            ->allowEmptyString('estado', false);

        $validator
            ->integer('tipo')
            ->requirePresence('tipo', 'create')
            ->allowEmptyString('tipo', false);
        
        $validator
            ->provider('proffer', 'Proffer\Model\Validation\ProfferRules')
            ->add('photo', 'extension', [
                'rule' => ['extension', ['gif','png','jpg']],
                'message' => 'La imagen debe tener extensión jpg, png o gif.',
            ])
            ->add('photo','fileSize',[
                'rule' => ['fileSize','<=','1MB'],
                'message' => 'El tamaño maximo de la imagen es de 1MB.',
            ]);
        
        $validator->allowEmptyString('passwordAnt', false, 'Debes ingresar tu contraseña anterior.');
        $validator->allowEmptyString('passwordNue', false, 'Debes colocar tu nueva contraseña.');
        $validator->allowEmptyString('passwordNue2', false, 'Debes confirmar tu nueva contraseña');
        
        $validator->add('passwordAnt','custom',['rule'=>  function($value, $context){
                        $usuario = $this->get($context['data']['id']);
                        if ($usuario) {
                            if ((new DefaultPasswordHasher)->check($value, $usuario->password)) { 
                                return true; 
                            }
                        }
                        return false;
                        },'message' => 'Las contraseñas no coincide.']);
                        
        $validator
            ->scalar('passwordNue')
            ->maxLength('passwordNue', 60, 'La contraseña sólo puede tener 60 caracteres.')
            ->minLength('passwordNue', 8, 'La contraseña debe tenermás de 8 caracteres.')
            ->allowEmptyString('passwordNue', false)
            ->add('passwordNue', 'validFormat', [
                'rule' => array('custom', '/(?=.*[A-ZÁÉÍÓÚÑ])(?=.*[a-záéíóúñ])(?=.*[0-9])(?=.*[$.,!¡_?¿%&])/m'),
                'message' => 'El valor ingresado no es válido. Debe tener al menos una letra mayúscula, una letra minúscula, un número y un caracter especial ($.,!¡_¿?%&.)']);
                        
        return $validator;

    }

    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['correo']));

        return $rules;
    }
}
