<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MovimientoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MovimientoTable Test Case
 */
class MovimientoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MovimientoTable
     */
    public $Movimiento;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Movimiento'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Movimiento') ? [] : ['className' => MovimientoTable::class];
        $this->Movimiento = TableRegistry::getTableLocator()->get('Movimiento', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Movimiento);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
